package tap

import org.scalacheck.Gen
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object AgendaPropertyTest extends Properties("Agenda Properties") {

  // ------------------  Generate nurse list --------------------
    // choose 1 to 4 of 5 skills
  val genNurseSkill : Gen[Seq[Int]] = for (
    n <- Gen.choose(1,4) ;
    skills <- Gen.pick(n,(1 until 6))
  ) yield skills
    // Nurse receives id (ids are seqential)
  def genNurse(id: Int): Gen[Nurse] = for(
    ls <- genNurseSkill
  ) yield Nurse(id,ls.toList)

    // Generate a maximum of 6 nurses with sequential ids
  val genNurseList : Gen[List[Nurse]] =
    Gen.choose(1,6).flatMap( n =>
      Gen.sequence[List[Nurse], Nurse]((1 until n+1).map(id => genNurse(id)))
    )
  // ---------------------------------------------------------------

  // ------------------  Generate period list --------------------
    // Generate one skill present in the nurses' list
  def genSkill(ln: List[Nurse]) : Gen[Int] = {
    Gen.oneOf(ln.flatMap(_.lSkills).distinct)
  }
    // Generate one period
    // To be valid, period depends on how many nurses have a particular skill
    // v is used to invalidate the period (if positive)
  def genPeriod(ln: List[Nurse], id:String, v: Int) : Gen[Period] = for(
    s <- genSkill(ln) ;
    n = ln.filter(_.lSkills.contains(s)).size
  ) yield Period(id.mkString,n+v,s)

    // Period list is composed only of "morning","afternoon" e "night"
  def genPeriodList(ln: List[Nurse], v: Int) : Gen[List[Period]] =
    Gen.sequence[List[Period],Period](List("morning","afternoon","night").map(s => genPeriod(ln,s,v)))

  // ---------------------------------------------------------------

  // A valid nurse list and period list is generated with 0 nurses added to the maximum for each skill
  val genValidPeriodAndNurse : Gen[(List[Nurse],List[Period])] = for(
    ln <- genNurseList ;
    lp <- genPeriodList(ln,0)
  ) yield (ln,lp)

  property("test Valid Schedule") =
    forAll(genValidPeriodAndNurse) { case (ln,lp) =>
      Agenda(ln,lp).schedule.isDefined
    }

  // An invalid nurse list and period list is generated with 1 to 10 nurses added to the maximum for each skill
  val genInvalidPeriodAndNurse : Gen[(List[Nurse],List[Period])] = for(
    ln <- genNurseList ;
    i <- Gen.choose(1,10) ;
    lp <- genPeriodList(ln,i)
  ) yield (ln,lp)

  property("test invalid Schedule") =
    forAll(genInvalidPeriodAndNurse) { case (ln,lp) =>
      !Agenda(ln,lp).schedule.isDefined
    }

}
