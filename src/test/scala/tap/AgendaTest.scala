package tap

import org.scalatest.FunSuite

class AgendaTest extends FunSuite {

  test("testSchedule") {
    val ln = List( Nurse(1,List(1,2)), Nurse(2,List(2,3)), Nurse(3,List(1,3)) )
    val lp1 = List( Period("manha",1,2), Period("tarde",2,1), Period("noite",2,3))
    val lp2 = List( Period("manha",1,2), Period("tarde",2,1), Period("noite",3,1))
    val lp3 = List( Period("manha",1,2), Period("tarde",2,1), Period("noite",1,4))


    val ls = Some(List(
      Shift(Nurse(1,List(1, 2)),Period("manha",1,2)),
      Shift(Nurse(1,List(1, 2)),Period("tarde",2,1)), Shift(Nurse(3,List(1, 3)),Period("tarde",2,1)),
      Shift(Nurse(2,List(2, 3)),Period("noite",2,3)), Shift(Nurse(3,List(1, 3)),Period("noite",2,3))
    ))
    assert(Agenda(ln,lp1).schedule === ls)

    assert(Agenda(ln,lp2).schedule === None)

    assert(Agenda(ln,lp3).schedule === None)
  }

}
