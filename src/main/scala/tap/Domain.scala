package tap

case class Nurse(id: Int, lSkills: List[Int])

case class Period(id: String, nQuantity: Int, nSkill: Int)

case class Shift(n: Nurse, p:Period)

case class Agenda(ln: List[Nurse], lp: List[Period]) {

  def scheduleOnePeriod(ln: List[Nurse],p: Period): Option[List[Shift]] = {
    val lnp = ln.filter(_.lSkills.contains(p.nSkill)).take(p.nQuantity)
    if (lnp.size != p.nQuantity) None else Some(lnp.map(n => Shift(n,p) ))
  }

  def schedule: Option[List[Shift]] =
    lp.foldLeft[Option[List[Shift]]](Some(List())) { case (ols,p) =>
      ols.flatMap(ls => scheduleOnePeriod(ln,p).map(lsp => ls ::: lsp))
    }
}
